{src, version, npmDepsHash, buildNpmPackage}:
buildNpmPackage {
    pname = "solawi-bedarf-frontend";
    inherit version src npmDepsHash;
    sourceRoot = "source/frontend";
      
    installPhase = ''
        mkdir -p $out/frontend
        cp -r dist/* $out/frontend
    '';
    meta = {
        description = "Frontend for the solawi-bedarf app";
        license = "AGPL-3.0";
        homepage = "";
    };
}