{src, version, npmDepsHash, buildNpmPackage, runtimeShell, nodejs}:
buildNpmPackage {
    pname = "solawi-bedarf-backend";
    inherit version src npmDepsHash;
    sourceRoot = "source/backend";

    patches = [
        ./001-backend-package.patch
        ./002-database-use-url.patch
        ./003-server-port.patch
    ];

    installPhase = ''
        mkdir -p $out/bin
        cp -r dist $out
        cp -r node_modules $out
        cat <<EOF > $out/bin/plantage-be
        #!${runtimeShell}
        exec ${nodejs}/bin/node $out/node_modules/.bin/ts-node $out/dist/backend/src/index.js
        EOF
        chmod +x $out/bin/plantage-be
    '';

    meta = {
        description = "Backend for the solawi-bedarf app";
        license = "AGPL-3.0";
        homepage = "";
    };
}
