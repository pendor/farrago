{callPackage, fetchFromGitHub, symlinkJoin}:
let
    name = "solawi-bedarf";
    version = "0.0.0";
    versions = {
        "0.0.0" = {
            commit = "9e4001871bb081c677815752612122ac03f41f5a";
            hash  = "sha256-+rKIt7ZbOPjscJdTIN2YJA+7nX1+5NQa1cus/DvdgTA=";
            backendNpmDepsHash = "sha256-1cQBhBn2gPO6vDjLGOnzI4eCXIi6RIFJVlFJdvYo0GQ=";
            fronendNpmDepsHash = "sha256-vF5B1OQajwQKbAQVxI2aPanGlUSoRTKpLLeZR5q1fbA=";
        };
    };
    src = fetchFromGitHub {
        owner = "lebenswurzel";
        repo = "solawi-bedarf";
        rev = versions.${version}.commit;
        sha256 = versions.${version}.hash;
    };
    backend = callPackage ./backend.nix {
        inherit src version;
        npmDepsHash = versions.${version}.backendNpmDepsHash;
    };
    frontend = callPackage ./frontend.nix {
        inherit src version;
        npmDepsHash = versions.${version}.fronendNpmDepsHash;
    };
in symlinkJoin {
    name = "solawi-bedarf";
    paths = [
        frontend
        backend
    ];
}
