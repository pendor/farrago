#libsForQt5.callPackages
{ stdenv, lib, qtbase, wrapQtAppsHook, fetchFromGitHub, ffmpeg-full, freac,
  coreutils
}: 

stdenv.mkDerivation {
  pname = "hoerbert-software";
  version = "2.1.5";

  buildInputs = [ qtbase ];
  nativeBuildInputs = [ wrapQtAppsHook ];

  src = fetchFromGitHub {
    owner = "winzkigermany";
    repo = "hoerbert-software";
    rev = "2.1.5";
    sha256 = "sha256:1n4m4x46297gjsnhidf66hsval6zj14y4cvvdz20vmmfm7pxi07g";
  };

  configurePhase = "qmake";

  installPhase = ''
    install -m775 -D ../Build/hoerbert $out/bin/hoerbert
  '';

  patchPhase = ''
    sed -i "s#FFMPEG_PATH\s*=.*#FFMPEG_PATH = \"${ffmpeg-full}/bin/ffmpeg\";#"  hoerbert/main.cpp
    sed -i "s#FFPROBE_PATH\s*=.*#FFPROBE_PATH = \"${ffmpeg-full}/bin/ffprobe\";#"  hoerbert/main.cpp
    sed -i "s#FFPLAY_PATH\s*=.*#FFPLAY_PATH = \"${ffmpeg-full}/bin/ffplay\";#"  hoerbert/main.cpp
    sed -i "s#FREAC_PATH\s*=.*#FREAC_PATH = \"${freac}/bin/freaccmd\";#"  hoerbert/main.cpp
    sed -i "s#SYNC_PATH\s*=.*#SYNC_PATH = \"${coreutils}/bin/sync\";#"  hoerbert/main.cpp
  '';

}
