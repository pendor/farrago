{ stdenv, lib, autoPatchelfHook, fetchurl }:

let
  version = "v2.1.2";

  src = fetchurl {
      url = "https://github.com/prysmaticlabs/prysm/releases/download/${version}/beacon-chain-${version}-linux-amd64";
      sha256 = "1vklhd7l71450wgqz3fwc1id9s4q82yg31gh4dgvra65m0l4yf9c";
      #2.1.1 sha256 = "06wwjv4sxxlkizl910h6jibdniy0hiaa9sc97ynxr6ca56i49sjb";
      #2.1.0 sha256 = "1w9af477dczqdvn1gx7x7ccfzm1rz4zy4ahlhzjiqm79wr15236s";
      #2.0.5 sha256 = "sha256:1f8i1vfp2hvr42fc2laa5h1gb0klby05a25azwzk3j7ynrmjzfpd";
      #2.0.6 sha256 = "sha256:0a38yylz5mkqb1ffd8yj402hv22q0wp03bgl58wj50ny5dj1yfds";
  };
  src_validator = fetchurl {
      url = "https://github.com/prysmaticlabs/prysm/releases/download/${version}/validator-${version}-linux-amd64";
      sha256 = "11yq80i933wldahvkjhw1nk4ry0n58i7l9da5bdgn11gin111abj";
      #2.1.1 sha256 = "1w6jz0nn4vb54ympf06yvlv00vmbc4vvwc5sbzgm9zwmjdwxbz8l";
      #2.1.0 sha256 = "1j7abnhf9isksmk3lnm7pfzaz1ky41j972m9d2yw4lvscvlyx2yc";
      #2.0.5 sha256 = "sha256:0i1m3j9xcrq61c4h8s0plc2zap8s3wf0yd919n8han2xlvqnl7ax";
      #2.0.6 sha256 = "sha256:0f5g6012amg1i78s8dary5vs168yy671j9q3bc2sll0m6vkyw660";
  };

in

stdenv.mkDerivation {
  name = "prysm-${version}";
  inherit version;

  inherit src;
  
  nativeBuildInputs = [
    autoPatchelfHook
    stdenv.cc.cc.lib
  ];

  buildInputs = [
  ];

  dontUnpack = true;

  installPhase = ''
    install -m755 -D ${src} $out/bin/prysm-beacon-chain
    install -m755 -D ${src_validator} $out/bin/prysm-validator
  '';

  meta = with lib; {
    homepage = https://prylabs.net;
    description = "Prysm Eth2 Tools";
    platforms = platforms.linux;
    maintainers = "Mattis Hasler";
  };

}
