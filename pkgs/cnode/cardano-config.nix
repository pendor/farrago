{ fetchurl, linkFarmFromDrvs, writeText, lib, net ? "mainnet"}:
with let
  src_base = "https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1";
  shelley_genesis_hash = {
    testnet = "19ng3grvz3niashggh0vblf5hw2symp34l4j5d25r7diyz8rlc2f";
    mainnet = "0qb9qgpgckgz8g8wg3aa9vgapym8cih378qc0b2jnyfxqqr3kkar";
  };
  alonzo_genesis_hash = {
    mainnet = "0234ck3x5485h308qx00kyas318dxi3rmxcbksh9yn0iwfpvycvk";
  };
  byron_genesis_hash = {
    testnet = "11vxckfnsz174slr7pmb5kqpy8bizkrqdwgmxyzl7fbvj2g178yw";
    mainnet = "1ahkdhqh07096law629r1d5jf6jz795rcw6c4vpgdi5j6ysb6a2g";
  };
  config_hash = {
    testnet = "0mz0lzj4f3z7n9v52pc0zmm3l326q3d8k026cpc5rmp47xz47lly";
    mainnet = "0s0bjpd1n2j0z1x07fbcdd4gzsmcya2ci1cys28wksb94v32zihx";
  };
  topology_hash = {
    testnet = "0kqm5dzl4iynabzsn9br2gdsiqy3wc9cp3iga6knwr9d3ndr3kyb";
    mainnet = "0c2p6vznyl96l2f1f5phkhdwckvy3d8515apgpl744jxym7iihks";
  };
in {
  alonzo_genesis = fetchurl {
    url = "${src_base}/${net}-alonzo-genesis.json";
    sha256 = alonzo_genesis_hash.${net};
  };
  shelley_genesis = fetchurl {
    url = "${src_base}/${net}-shelley-genesis.json";
    sha256 = shelley_genesis_hash.${net};
  };
  byron_genesis = fetchurl {
    url = "${src_base}/${net}-byron-genesis.json";
    sha256 = byron_genesis_hash.${net};
  };
  config_raw = fetchurl {
    url = "${src_base}/${net}-config.json";
    sha256 = config_hash.${net};
  };
  topology_raw = fetchurl {
    url = "${src_base}/${net}-topology.json";
    sha256 = topology_hash.${net};
  };
};
with {
  config_json = lib.importJSON  config_raw;
};
with let
  config_mod = {
    AlonzoGenesisFile = alonzo_genesis;
    ByronGenesisFile = byron_genesis;
    ShelleyGenesisFile = shelley_genesis;
  };
in {
    config_file = writeText "config-file.json" (builtins.toJSON (config_json // config_mod));
};
linkFarmFromDrvs "cardano-config" [ alonzo_genesis shelley_genesis byron_genesis config_raw topology_raw config_file ]
