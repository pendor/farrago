#!/usr/bin/env python3

import argparse
import subprocess
import time
from pathlib import Path
import shutil
import signal
import requests
import json

API_PUSH = "https://api.clio.one/htopology/v1/"
API_FETCH = "https://api.clio.one/htopology/v1/fetch/" #?max=${MAX_PEERS}&magic=${NWMAGIC}&ipv=${IP_VERSION}"

REQUEST_BLOCKNO = "http://{EKG_HOST}:{EKG_PORT}/"
REQUEST_BLOCKNO_HEADERS = {"Accept": "application/json"}
EKG_PORT = 12788
EKG_HOST = 'localhost'

NETWORK_MAGIC = 764824073 #mainnet
CNODE_PORT = 3001
CNODE_HOST = "vmd80087.contaboserver.net"
CNODE_VALENCY = 2
CNODE_MAXPEERS = 10

TOPO_FILE = "/var/lib/cnode/mainnet/topology.json"
TOPO_FALLBACK = None
TOPO_STATIC = "/var/lib/cnode/mainnet/static.json"

def get_blockno():
    try:
        r = requests.get(REQUEST_BLOCKNO.format(EKG_HOST=EKG_HOST, EKG_PORT=EKG_PORT), headers=REQUEST_BLOCKNO_HEADERS)
        data = r.json()
        print(r.text)
        num = int(data['cardano']['node']['metrics']['blockNum']['int']['val'])
        return num
    except:
        return None

def api_gettopology(blockNo):
    params = {
        "max": CNODE_MAXPEERS,
        "magic": NETWORK_MAGIC,
        "ipv": 4
    }
    r = requests.get(API_FETCH, params=params)
    print(r)
    #load static
    if TOPO_STATIC is not None:
        try:
            with open(TOPO_STATIC, "r") as fh:
                data = json.load(fh)
                prod = data['Producers']
        except:
            prod = {}
    else:
        prod = {}
    #is response any good?
    data = r.json()
    if 'Producers' in data:
        prod.extend(data['Producers'])
    elif TOPO_FALLBACK is not None: #fallback
        with open(TOPO_FALLBACK, "r") as fh:
            data = json.load(fh)
            prod.extend(data['Producers'])
    #
    if prod == {}:
        print("no producer info - will not write file")
        return
    data = {'Producers': prod}
    with open(TOPO_FILE, "w") as fh:
        json.dump(data, fh)

def api_register(blockNo):
    params = {
        "port": CNODE_PORT,
        "blockNo": blockNo,
        "valency": CNODE_VALENCY,
        "magic": f"${NETWORK_MAGIC}${CNODE_HOST}",
    }
    r = requests.get(API_PUSH, params=params)
    print(r)
    print(r.text)

def loadparams():
    parser = argparse.ArgumentParser()
    parser.add_argument("command", choices=['register', 'topo'], default='register')
    parser.add_argument("--topo-out", nargs='?', type=str)
    parser.add_argument("--topo-fallback", nargs='?', type=str)
    parser.add_argument("--topo-static", nargs='?', type=str)
   
    args = parser.parse_args()
    global TOPO_FILE; TOPO_FILE = args.topo_out
    global TOPO_FALLBACK; TOPO_FALLBACK = args.topo_fallback
    global TOPO_STATIC; TOPO_STATIC = args.topo_static
    global CMD; CMD = args.command

def main():
    loadparams()
    blockno = get_blockno()
    if blockno is None:
        print("cannot get blockno")
        return
    print(f"block #{blockno}")
    if CMD == 'register':
        api_register(blockno)
    elif CMD == 'topo':
        api_gettopology(blockno)
    else:
        print("unknown command")


if __name__ == '__main__':
    main()
