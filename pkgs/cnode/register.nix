{ pkgs, stdenv }:

stdenv.mkDerivation {
  name = "cnode-register";
  src = builtins.path {
    name = "register-script";
    path = ./py;
  };
  buildInputs = [
    (pkgs.python3.withPackages (pythonPackages: with pythonPackages; [
      requests
    ]))
  ];
  installPhase = ''
    mkdir -p $out/bin $out/py
    cp register.py $out/py
    ln -s $out/py/register.py $out/bin/cnode-register
  '';
}
