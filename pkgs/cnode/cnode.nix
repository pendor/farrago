{
  lib,
  writeScriptBin,
  writeText,
  bash,
  coreutils,
  cardano-node,
  cardano-config,
  net ? "mainnet",
  libdir ? "/var/lib/cnode",
  rundir ? "/run/cnode",
  addr ? "0.0.0.0",
  port ? 3001,
  poolkeys ? null,
  rtsparams ? null
}:

writeScriptBin "cnode-start" (''
  #!${bash}/bin/bash
  ${coreutils}/bin/mkdir -p ${libdir}/${net}/db
  ${coreutils}/bin/mkdir -p ${rundir}/${net}
  ${cardano-node}/bin/cardano-node run
'' +
(if rtsparams == null then "" else ''
  +RTS ${rtsparams} -RTS
'' +
''
  --topology ${libdir}/${net}/topology.jsons \
  --database-path ${libdir}/${net}/db \
  --socket-path ${rundir}/${net}/node.socket \
  --host-addr ${addr} \
  --port ${toString port} \
  --config ${cardano-config}/config-file \
'' +
(if poolkeys == null then "" else ''
  --shelley-kes-key ${poolkeys.kes} \
  --shelley-vrf-key ${poolkeys.vrf} \
  --shelley-operational-certificate ${poolkeys.cert}
''))
