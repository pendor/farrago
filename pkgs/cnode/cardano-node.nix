{ stdenv, fetchurl }:

let
  version = "1.31.0";

  src = fetchurl {
      #1.33.1
      url = "https://hydra.iohk.io/build/9941151/download/1/cardano-node-1.33.0-linux.tar.gz";
      sha256 = "sha256:0hsmg4v235s4in1qcw3w60hx6kja2zp7zvcrcvsvig3r5xjvqzzb";
      #1.32.1
      #url = "https://hydra.iohk.io/build/9639640/download/1/cardano-node-1.32.1-linux.tar.gz";
      #sha256 = "0w8y3hhhjwwmk9gzbwv1n1cwx8sabr6zwg9y9qk82n10h4mwpzx2";
      #1.31.0
      #url = "https://hydra.iohk.io/build/8128436/download/1/cardano-node-1.31.0-linux.tar.gz";
      #sha256 = "1lq6ql6lsmsrwf0yj7rikr12aga6sfa2i5jgpahr1qzizkdgqgz1";
      #1.29.0
      #url = "https://hydra.iohk.io/build/7408438/download/1/cardano-node-1.29.0-linux.tar.gz";
      #sha256 = "1q08bf0ndk6d0052fmjvra44jdsddkkszzgh3gxrqvnkx9fvc5av";
  };
in

stdenv.mkDerivation {
  name = "cardano-node-${version}";
  inherit version;

  inherit src;
  
  buildInputs = [
  ];

  installPhase = ''
    install -m775 -D ../cardano-node $out/bin/cardano-node
    install -m775 -D ../cardano-cli $out/bin/cardano-cli
  '';

}
