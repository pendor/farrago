{
  installDir,
}:

let
  installer = "installer/Xilinx_Unified_2021.1_0610_2318_Lin64.bin";
  pkgs = import (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/5e701625de8d68c1436c71f69c098b33b0445808.tar.gz";
  }) {};
in

{
  vivado = pkgs.buildFHSUserEnv {
    name = "vivado";
    targetPkgs = pkgs: (with pkgs;
      [
        xorg.libX11
        xorg.libXext
        xorg.libXrender
        xorg.libXtst
        xorg.libXi
        xorg.libXft
        xorg.libxcb
        freetype
        fontconfig
        ncurses5
        zlib
      ]);
    runScript = "${installDir}/bin/vivado";
  };
  xvlog = pkgs.buildFHSUserEnv {
    name = "xvlog";
    targetPkgs = pkgs: [ pkgs.ncurses5 ];
    runScript = "${installDir}/bin/xvlog";
  };
  xvhdl = pkgs.buildFHSUserEnv {
    name = "xvhdl";
    targetPkgs = pkgs: [ pkgs.ncurses5 ];
    runScript = "${installDir}/bin/xvhdl";
  };
  xelab = pkgs.buildFHSUserEnv {
    name = "xelab";
    targetPkgs = pkgs: [ pkgs.ncurses5 pkgs.gcc ];
    runScript = "${installDir}/bin/xelab";
  };
  xsc = pkgs.buildFHSUserEnv {
    name = "xsc";
    targetPkgs = pkgs: [
      pkgs.ncurses5
    ];
    extraOutputsToInstall = [ "dev" ];
    runScript = "${installDir}/bin/xsc";
  };
  xsim = pkgs.buildFHSUserEnv {
    name = "xsim";
    targetPkgs = pkgs: (with pkgs; [
      xorg.libX11
      xorg.libXext
      xorg.libXrender
      xorg.libXtst
      xorg.libXi
      xorg.libXft
      xorg.libxcb
      freetype
      fontconfig
      ncurses5
      zlib
    ]);
    runScript = "${installDir}/bin/xsim";
  };
  cmd = pkgs.buildFHSUserEnv {
    name = "cmd";
    targetPkgs = pkgs: [ pkgs.ncurses5 ];
    runScript = "bash";
  };
  install = pkgs.buildFHSUserEnv {
      name = "XilinxInstaller";
      targetPkgs = pkgs: (with pkgs;
      [
        xorg.libX11
        xorg.libXext
        xorg.libXrender
        xorg.libXtst
        xorg.libXi
        xorg.libXft
        xorg.libxcb
        freetype
        fontconfig
        ncurses5
        zlib
      ]);
    runScript = installer;
  };
}
