{ cups-kyocera, fetchzip }:

cups-kyocera.overrideAttrs (old: {
  src = fetchzip {
    # this site does not like curl -> override useragent
    curlOpts = "-A ''";
    url = "https://www.kyoceradocumentsolutions.de/content/download-center/de/drivers/all/LinuxDrv_1_1203_FS_1x2xMFP_zip.download.zip";
    sha256 = "0z1pbgidkibv4j21z0ys8cq1lafc6687syqa07qij2qd8zp15wiz";
  };
})