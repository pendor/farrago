{ vscode-extensions, vscode-with-extensions, vscode-utils }:

vscode-with-extensions.override {
  vscodeExtensions = (with vscode-extensions; [
    brettm12345.nixfmt-vscode
    #kamadorueda.alejandra
    mhutchie.git-graph
    ms-python.python
    ms-python.vscode-pylance
    ms-vscode.cpptools
    ms-vscode-remote.remote-ssh
    ms-azuretools.vscode-docker
    #ms-toolsai.jupyter
    bbenoist.nix
    redhat.vscode-yaml
    james-yu.latex-workshop
    github.copilot
  ])
  ++ vscode-utils.extensionsFromVscodeMarketplace [
  {
    name = "git-line-blame";
    publisher = "carlthome";
    version = "0.8.0";
    sha256 = "sha256-voMnpi4XcclmW49/HeKUctDGOYAdLp8dio5nr7kEMpg=";
  }
  {
    name = "VerilogHDL";
    publisher = "mshr-h";
    version = "1.5.3";
    sha256 = "1har7q0flqnx5q74nj3gn8l80aibmnn4xyscddbim5i5yqdx45g0";
  }
  {
    name = "vscode-arduino";
    publisher = "vsciot-vscode";
    version = "0.4.6";
    sha256 = "1951fxyj80cwd8x295z9farynsjpb3cg2wbf58bx90blqfrqg6vl";
  }
  {
    name = "grammarly";
    publisher = "znck";
    version = "0.23.9";
    sha256 = "sha256-82xo8saq1vyMUyr1VDlEOqi4QY2rYlhGgCpka5zhkJo=";
  }
  {
    name = "tcl";
    publisher = "bitwisecook";
    version = "0.4.0";
    sha256 = "sha256:0fw1akw5szpw8ccxsvm463gqyr3as7mcvaln7xhjymy7385zh9dl";
  }
  {
    name = "volar";
    publisher = "Vue";
    version = "2.0.10";
    sha256 = "sha256-L5z7Rg8ybHTCGwO9HHExg0BfTBvO34Uz40NrNpzDgBk=";
  }
  {
    name = "surfer";
    publisher = "surfer-project";
    version = "0.1.1";
    sha256 = "sha256-nGr3A1MQrq66CQMmOVXpFYkfpp9NCH4YrWbVcoaIKPY=";
  }
  {
    name = "debugpy";
    publisher = "ms-python";
    version = "2024.6.0";
    sha256 = "sha256-VlPe65ViBur5P6L7iRKdGnmbNlSCwYrdZAezStx8Bz8=";
  }
  ]
  ;
}
