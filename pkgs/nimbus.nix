{ stdenv, lib, autoPatchelfHook, fetchurl }:

let
  version = "24.2.2";
  srcs = {
    "24.2.2" = { fprint = "fc9c72f0"; hash = "sha256-onUZd0wdOxuZL0kd3oVUNOQ8fhjoBEgTU64Mdff3tC0="; };
    "23.10.0" = { fprint = "8b07f4fd"; hash = "sha256-Hqe2/8Tky4QgsJRsiI61NvaxQxZV13kSHtHeWrHP1KA="; };
    "23.6.1" = { fprint = "187e1a06"; hash = "sha256-GCcfaJKwYIlPc2ISkMKlra/Tbb8Dapf1pQMnAENG/dA="; };
    "23.5.1" = { fprint = "4842c9d1"; hash = "sha256-bTTT4yiDaTw4703G4lK+vOGMKCSdiRlzTmg7q3ovQGI="; };
    "23.5.0" = { fprint = "40253a76"; hash = "sha256-YgpEsL8c3m7IHLDXNsLz+tvTETIZ8xMbcwn0Ms1XCxQ="; };
    "23.4.0" = { fprint = "e5a5731b"; hash = "sha256-L/IuWx9puvjD+W5c1onMPXrhlV6D/X2EIoU7s1gglnI="; };
    "23.3.2" = { fprint = "6c0d756d"; hash = "sha256-5RamgB6BO0j8+T+i2qpiAoNftdUR0OBG8gJnD0ZQ16Q="; };
    "23.3.0" = { fprint = "17c0eeee"; hash = "sha256-yuBG5alh0bQCDPJUTPgwVTzmqIcSDVSYcoQMYgEdLIQ="; };
    "23.2.0" = { fprint = "6b9381ef"; hash = "sha256-wNQ7u8akqRhQnPh2byFMf2cOMY02fNXAHwE6tFO9FY8="; };
    "23.1.1" = { fprint = "1ea102b0"; hash = "sha256-dbM1q1lphXZeeaMlsSA/3/pc0hfSW6EAs+FaRB4wCcY="; };
    "22.12.0" = { fprint = "f6a5a5b1"; hash = "sha256-mni9oJDs4b2jvGwBZC48746HzK3sLIDvsOjH6Q8ufoc="; };
    "22.11.0" = { fprint = "356dd4ee"; hash = "sha256-eznTuFfg6sa0PmVRp2aaN222ImWyMFhKu/oj76NGlbM="; };
    "22.10.0" = { fprint = "2d6911d5"; hash = "sha256-BKWfHMFIM8VcuuH5HBr3dq/GL0z7KImCsf2uzSqXVEs="; };
    "22.9.1" = { fprint = "f7db5052"; hash = "sha256-OR+riNpQlppCmPwjYcHm3p5GSQ3gNcogZunewlqKV1k="; };
    "22.9.0" = { fprint = "667c3c97"; hash = "sha256-sFVcAI8JbfkXz8Z3UqN9UifyX657uGyOVsw1CLjIsio="; };
    "22.8.2" = { fprint = "3a8abd60"; hash = "sha256-5Nbd/R0uY1F+EzrtHDUDtT2YU3N/bIdvmpVx0pOPdJg="; };
    "22.6.1" = { fprint = "2444e994"; hash = "sha256:1gpv682r7rp8s53aq5yzrs7a57bzb0zszb67dvgq500m32442jm7"; };
    "22.4.0" = { fprint = "039bece9"; hash = ""; };
  };
  fprint = srcs.${version}.fprint;
  hash = srcs.${version}.hash;
  src = fetchurl {
      url = "https://github.com/status-im/nimbus-eth2/releases/download/v${version}/nimbus-eth2_Linux_amd64_${version}_${fprint}.tar.gz";
      hash = hash;
  };
in

stdenv.mkDerivation {
  name = "nimbus-${version}";
  inherit version;

  inherit src;
  
  nativeBuildInputs = [
    autoPatchelfHook
    stdenv.cc.cc.lib
  ];

  buildInputs = [
  ];

  installPhase = ''
    install -m755 -D build/nimbus_beacon_node $out/bin/nimbus_beacon_node
  '';

  meta = with lib; {
    homepage = https://nimbus.team;
    description = "Nimbus Eth2 Tool";
    platforms = platforms.linux;
    maintainers = [ "Mattis Hasler" ];
  };

}
