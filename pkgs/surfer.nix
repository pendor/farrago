{
  fetchgit,
  fontconfig,
  freetype,
  glib,
  gtk3,
  lib,
  libGL,
  libxkbcommon,
  openssl,
  pkg-config,
  rustPlatform,
  wayland,
  xorg,
}: let
  pname = "surfer";
  version = "0.1.0";
in
  rustPlatform.buildRustPackage rec {
    inherit pname version;
    src = fetchgit {
      url = "https://gitlab.com/surfer-project/surfer";
      rev = "v${version}";
      sha256 = "sha256-JaSL0Gy//gnzGPZkQUYQm2mDvbida51Fvyjb1zApEIw=";
      fetchSubmodules = true;
    };

    cargoHash = "";

    cargoLock = {
      lockFile = ./surferCargo.lock;
      outputHashes = {
        "codespan-0.12.0" = "sha256-3F2006BR3hyhxcUTaQiOjzTEuRECKJKjIDyXonS/lrE=";
        "egui_skia-0.5.0" = "sha256-Qsb3C+iiUpXTWuOnKEiNKpK61dYpgcz92iDIgvONN9o=";
        "tracing-tree-0.2.0" = "sha256-/JNeAKjAXmKPh0et8958yS7joORDbid9dhFB0VUAhZc=";
        "local-impl-0.1.0" = "sha256-w6kQ4wM/ZQJmOqmAAq9FFDzyt9xHOY14av5dsSIFRU0=";
      };
    };

    patchPhase = ''
      # Remove the dependency on the `x11` feature
      sed -i 's/eframe = "0.25.0"/eframe = { version="0.25.0",  features = ["glow", "x11", "default_fonts"], default-features = false}/' Cargo.toml
    '';

    buildInputs =
      [openssl fontconfig freetype wayland]
      ++ (with xorg; [libX11 libXcursor libXrandr libXi])
      ++ [libGL libxkbcommon glib gtk3];
    nativeBuildInputs = [pkg-config];

    postFixup = let
      rpathlib = [
        libGL
        libxkbcommon
        xorg.libX11
        xorg.libXcursor
        xorg.libXi
        xorg.libXrandr
        xorg.libXxf86vm
        xorg.libxcb
        wayland
      ];
    in ''
      patchelf --add-rpath "${lib.makeLibraryPath rpathlib}" $out/bin/surfer
    '';

    meta = with lib; {
      description = "A simple and fast web browser";
      license = licenses.eupl12;
    };
  }
