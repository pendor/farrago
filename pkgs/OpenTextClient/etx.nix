{ stdenv, buildFHSUserEnv, runCommand }:

let 
    etxlauncher = stdenv.mkDerivation {
        name = "etxlauncher";

        version = "1.0.0";

        src = ./ETXLauncher.tar.gz;

        installPhase = ''
            install -m775 -D bin/etxlauncher $out/bin/etxlauncher
            install -m664 -D conf/etxlauncher.desktop.template $out/template/etxlauncher.desktop.template
        '';
    };
in
let
    etxwrap = buildFHSUserEnv {
        name = "etxlauncherwrap";

        targetPkgs = pkgs: with pkgs; [
            etxlauncher
            gtk2-x11
            atk
            glib
            gnome2.pango
            gdk-pixbuf
            cairo
            freetype
            fontconfig
            xorg.libXext
            xorg.libXft
            xorg.libXi
            xorg.libXrender
            xorg.libX11
        ];

        runScript = "etxlauncher";
    };
    desktopfile = "$out/share/applications/etxlauncher.desktop";
in
runCommand "OpenTextClient" {} ''
    mkdir -p $out/bin
    ln -s ${etxwrap}/bin/etxlauncherwrap $out/bin/etxlauncher
    mkdir -p $out/share/applications
    cp ${etxlauncher}/template/etxlauncher.desktop.template ${desktopfile}

    sed -i 1d ${desktopfile}
    sed -i s#__ETXLAUNCHERHOME__#${etxwrap}# ${desktopfile}
    sed -i s#__LAUNCHBINARY__#etxlauncherwrap# ${desktopfile}
    sed -i s#__TERMINAL__#false# ${desktopfile}
''