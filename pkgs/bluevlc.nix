{ libbluray, vlc }:
let
    libblurayAacs = libbluray.override {
      withAACS = true;
      withJava = true;
      #withBDPlus = true;
    };
in vlc.override { libbluray = libblurayAacs; }

# get keydb.cfg from here: http://fvonline-db.bplaced.net/
# copy it to ~/.config/aacs/
# rename to KEYDB.cfg (case sensitive)
