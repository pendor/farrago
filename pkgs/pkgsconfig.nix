{lib}:
{
  permittedInsecurePackages = [
    #"xpdf-4.03"
    #"python3.9-mistune-0.8.4"
  ];
  #allowUnfree = true;
  allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "cups-kyocera"
    "vscode-with-extensions"
    "vscode"
    "vscode-extension-MS-python-vscode-pylance"
    "vscode-extension-ms-vscode-remote-remote-ssh"
    "vscode-extension-ms-vscode-cpptools"
    "vscode-extension-github-copilot"
    "vscode-extension-mhutchie-git-graph"
  ];
}

