{buildGoModule, fetchFromGitLab}:

let
  version = "0.6.0";
in
buildGoModule {
    pname = "grafana-matrix-forwarder";
    version = version;

    src = fetchFromGitLab {
        owner = "hectorjsmith";
        repo = "grafana-matrix-forwarder";
        rev = version;
        sha256 = "sha256-sGjVcoRvvqXdThwNtnIM7fpL7/5x85Z7Xff4f7uFlLQ=";
    } + "/src";

    vendorSha256 = "sha256-brNTIAX651Ao32bFvLB3KmtzzQqbvTNajwLoiUrJguM=";
}