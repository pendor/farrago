{
  rustPlatform,
  fetchFromGitHub
}:
let
    pname = "Bender";
    version = "v0.28.1";
in rustPlatform.buildRustPackage {
    inherit pname version;
    src = fetchFromGitHub {
        owner = "pulp-platform";
        repo = "bender";
        rev = version;
        hash = "sha256-eC4BY3ri73vgEtcXoPQ5NDknjZcPrKOzLo2vXWj4Adg=";
    };
    cargoHash = "sha256-QOz6JY4ThM0eDt0VFkHZUVBpcss+cKcgNKj8ojmYk7E=";
}