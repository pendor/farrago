{pkgs, ...}: let
  nixpkgs =
    import (fetchTarball {
      url = "https://github.com/nixos/nixpkgs/archive/cbe2cb9d6f8da40069ee192f598a843dab6e96df.tar.gz";
      sha256 = "sha256:0ckfysmvv2kjik2g0s8ghjzbd2k99x2b27rhih2qh7mq17qykfq0";
    }) {
      system = pkgs.system;
    };
in
  nixpkgs.chromium
