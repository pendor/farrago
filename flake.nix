{
  description = "personal package and config accumulation";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    #nimbus.url = "github:status-im/nimbus-eth2?submodules=1";
    nimbus = {
      type = "git";
      url = "https://github.com/status-im/nimbus-eth2.git";
      submodules = true;
    };
  };
  outputs = { self, nixpkgs, flake-utils, nimbus, }@inputs:
    #packages
    inputs.flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import inputs.nixpkgs {
          inherit system;
          config = import ./pkgs/pkgsconfig.nix { inherit (pkgs) lib; };
        };
      in {
        packages = {
          bender = pkgs.callPackage pkgs/bender.nix { };
          bluevlc = pkgs.callPackage pkgs/bluevlc.nix { };
          chromium = pkgs.callPackage pkgs/chromium.nix { };
          cups-kyocera = pkgs.callPackage pkgs/kyocera_override.nix { };
          etx = pkgs.callPackage pkgs/OpenTextClient/etx.nix { };
          hoerbert = pkgs.libsForQt5.callPackage pkgs/hoerbert.nix { };
          icarus = pkgs.callPackage pkgs/icarus.nix { };
          reth = pkgs.callPackage pkgs/reth.nix { };
          surfer = pkgs.callPackage pkgs/surfer.nix { };
          solawi-bedarf = pkgs.callPackage pkgs/solawi-bedarf { };
          vscode = pkgs.callPackage pkgs/vscode.nix { };
        } // pkgs.lib.mapAttrs' (name: value: pkgs.lib.nameValuePair ("nimbus_" + name) value) inputs.nimbus.packages.${system};
      }) // {
        #templates
        templates = {
          latexmk = {
            path = ./templates/latexmk;
            description = "building documents with latexmk";
          };
        };
      };
}
