{
  description = "Paper";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-unstable;
    flake-utils.url = github:numtide/flake-utils;
  };
  outputs = { self, nixpkgs, flake-utils }:
    with flake-utils.lib; eachSystem defaultSystems (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      latexmk = import ./latexmk.nix { inherit pkgs; };
      name = "paper";
      tex = latexmk.tex {
        inherit pkgs;
        useIEEE = true;
        useBiblatex = true;
        texpkgs = tp: { inherit (tp)
          orcidlink environ;
        };
        #useMinted = true;
        #scheme = "medium";
      };
      doc = latexmk.doc {
        NoCCDerivation = pkgs.stdenvNoCC.mkDerivation;
        inherit tex name self;
        inherit (pkgs) coreutils;
        inherit (pkgs.lib) makeBinPath;
      };
    in {
      packages.default = doc;
      devShells.default = pkgs.mkShell {
        packages = tex;
        shellHook = ''
          export SOURCE_DATE_EPOCH=${toString self.lastModified}
          export FORCE_SOURCE_DATE=1
        '';
      };
    });
}



