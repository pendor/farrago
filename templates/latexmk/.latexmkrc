$pdf_mode = 1;
$aux_dir = 'build/';
$out_dir = 'build/';
$pdflatex = 'pdflatex \
-file-line-error \
-interaction=nonstopmode \
-synctex=1 \
-shell-escape \
%O %S';
@default_files = ('document.tex');
