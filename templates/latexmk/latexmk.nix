{ pkgs }:

{
  tex = {
    pkgs,
    scheme ? "small",
    texpkgs ? null,    # function: texlive -> {package=package; ...}
    useMinted ? false,
    useBiblatex ? false,
    useIEEE ? false
  }: with pkgs; [(texlive.combine (
      (if scheme == "full" then {inherit (texlive) scheme-full;}
      else if scheme == "medium" then {inherit (texlive) scheme-medium;}
      else if scheme == "small" then {inherit (texlive) scheme-small;}
      else if scheme == "basic" then {inherit (texlive) scheme-basic;}
      else if scheme == "minimal" then {inherit (texlive) scheme-minimal;}
      else if scheme == "teTex" then {inherit (texlive) scheme-tetex;}
      else if scheme == "ConTeXt" then {inherit (texlive) scheme-context;}
      else if scheme == "GUST" then {inherit (texlive) scheme-gust;}
      else throw "Unknown scheme: ${scheme}")
      // { inherit (texlive) latexmk; }
      // (if texpkgs != null then texpkgs texlive else {})
      // (if useMinted then {
        inherit (texlive)
          minted
          fvextra
          upquote
          catchfile
          xstring
          framed
          ;
      } else {})
      // (if useBiblatex then {inherit (texlive) biblatex biblatex-ieee;} else {})
      // (if useIEEE then {inherit (texlive) ieeeconf ieeetran;} else {})
      // {inherit (texlive) latexmk;}
  ))]
  ++ (if useBiblatex then [biber] else [])
  ++ (if useMinted then [bash which python310Packages.pygments] else [])
  ;

  doc = { NoCCDerivation, tex, coreutils, makeBinPath, name, self }: let
      buildInputs = [ coreutils ] ++ tex;
    in NoCCDerivation {
      inherit name buildInputs;
      src = self;
      phases = ["unpackPhase" "buildPhase" "installPhase"];
      SOURCE_DATE_EPOCH = toString self.lastModified;
      FORCE_SOURCE_DATE = 1;
      buildPhase = ''
        mkdir -p .cache/texmf-var
        latexmk -pretex="\pdftrailerid{}"
      '';
      installPhase = ''
        mkdir -p $out
        cp build/*.pdf $out/
      '';
    };
}

#% LuaTeX
#\pdfvariable suppressoptionalinfo 512\relax
#% pdfTeX
#\pdftrailerid{}
#% XeTeX
#\special{pdf:trailerid [
#    <00112233445566778899aabbccddeeff>
#    <00112233445566778899aabbccddeeff>
#]}